module gitlab.com/tuktech/pwa

go 1.12

require (
	github.com/aws/aws-sdk-go v1.23.9 // indirect
	github.com/oursky/aws-site-manager v0.0.0-20170322051139-f88ff99069a6 // indirect
)
